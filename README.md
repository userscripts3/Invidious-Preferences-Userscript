# Invidious-Preferences-Userscript


![Greasy Fork](https://img.shields.io/greasyfork/v/463714-invidious-preferences?style=flat-square)
![Greasy Fork](https://img.shields.io/greasyfork/dt/463714-invidious-preferences?style=flat-square)
![GitHub](https://img.shields.io/github/license/MintMain21/Invidious-Preferences-Userscript?style=flat-square) 

This Userscript (compatible with browser extensions such as Tampermonkey) is for enforcing universal Invidious preferences through URL Parameters. 

To install it, go [here](https://greasyfork.org/en/scripts/463714-invidious-preferences) and click "install".

To change the settings enforced, all you need to do is change the setting strings. For more information about URL Parameters, see [here](https://docs.invidious.io/url-parameters/)

It is recommended that you use this script in combination with either the following scripts ([Privacy Redirector](https://github.com/dybdeskarphet/privacy-redirector) and [Simple Sponsor Skipper](https://codeberg.org/mthsk/userscripts/src/branch/master/simple-sponsor-skipper)) or their browser extension alternatives ([LibRedirect](https://libredirect.github.io/) and [SponsorBlock](https://sponsor.ajay.app/)).

If anyone would like to port this feature either to it's own dedicated browser extension or as a feature for a pre-existing extension, it's under the MIT licence so you shoul be free to do so. Feel free to credit me and notify me so that I can see it in action.
